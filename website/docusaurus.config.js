/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Ensi',
  tagline: 'Documentation',
  url: 'https://ensi-platform.gitlab.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'ensi-platofrm',
  projectName: 'ensi-platform.gitlab.io', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Ensi',
      logo: {
        alt: 'Ensi logo',
        src: 'img/logo.png',
      },
      items: [
        {
          to: 'docs/tech/stack',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [],
      copyright: `Copyright © ${new Date().getFullYear()} Ensi.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/ensi-platform/ensi-platform.gitlab.io/-/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/ensi-platform/ensi-platform.gitlab.io/-/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
