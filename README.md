# ensi-platform.gitlab.io

В данном репозитории находится документация платформы, доступная на домене https://ensi-platform.gitlab.io 
В качестве движка используется [Docusaurus](https://docusaurus.io/), в качестве способа публикации - [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)

## Внесение изменений в документацию

Есть два способа внести изменения:
1. Через web-интерфейс Gitlab
2. Через локалку

Первый вариант удобен в случае мелких правок.
Второй - во всех остальных.

В корне репозитория лежит файл [markdown-features.mdx](./markdown-features.mdx) в котором можно посмотреть на дополнительные фичи которые можно применять в mdx файлах.

## Изменение через web-интерфейс

Ищем нужную доку тут https://gitlab.com/ensi-platform/ensi-platform.gitlab.io/-/tree/master/website/docs
Редактируем, сохраняем.

Для изменения левого меню необходимо отредактировать файл https://gitlab.com/ensi-platform/ensi-platform.gitlab.io/-/blob/master/website/sidebars.js 
## Изменение через локалку

Для внесения изменений через локалку вам потребуется 3 вещи:
1. [git](https://git-scm.com/downloads)
1. [nodejs](https://nodejs.org/en/)
1. Текстовый редактор на ваш выбор

Первым делом нужно склонировать репозиторий и установить зависимости:

```sh
git clone git@gitlab.com:ensi-platform/ensi-platform.gitlab.io.git
cd ensi-platform.gitlab.io/website
npm install
```

После этого можно локально запустить сервис документации:

```sh
npm start
```

Документация должна быть доступна по адресу `http://localhost:3000`

Вносим изменения в `.md` файлы документации, проверяем что всё соответствует нашим ожиданиям.  

Для сохранения работы её нужно закоммитить и запушить в Gitlab  

```sh
git add -A 
git commit -m "<какое-то ваше сообщение об изменении в документации>"
git push origin master
```

## Отгрузка изменения на домен ensi-platform.gitlab.io

Отгрузка изменений на ensi-platform.gitlab.io происходит в автоматическом режиме после того как в ветку master на Gitlab попал новый код  не зависимо от того сделали ли вы изменение через web-интерфейс или запушили его с локалки.  
Для того чтобы изменение применилось нужно время, обычно это несколько минут.  
Статус последней отгрузки можно найти на странице https://gitlab.com/ensi-platform/ensi-platform.gitlab.io/-/jobs  